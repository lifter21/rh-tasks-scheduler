**Job Scheduler**
=================

**Node.js version >= 7.6 and NPM >= 4**

**MongoDB version >= 3.2**

 
==================

_**core libs**_:

  * `koa` (app and )
  * `mongoose` (MongoDB orm/odm)
  * `node-ssh` (connecting to host via ssh)
  * `node-schedule` (job scheduling)
  * ...etc(`lodash`, `cron-parser`, `dotenv`)
  
### Quick start ###

1. Clone app repository.

2. Run in terminal `npm i`

3. Create `.env` file: `cp .env.example .env` or just add necessary configuration in `config/default.js`

4. Run `npm start`

4.1 You can also run app with Docker: 
 
`docker build -t tasks-scheduler .`

`docker run -d -p 3000:8080 --name tasks-scheduler`

4.2 You can run app either by pm2: `pm2 start process.yml` or `npm run start-pm2`