'use strict';

const gulp = require('gulp');
const nodemon = require('gulp-nodemon');

const config = require('config');

process.on('uncaughtException', function(err) {
  console.error(err.message, err.stack, err.errors);
  process.exit(255);
});

gulp.task('nodemon', () => {
  nodemon({
    execMap: {
      js: 'node'
    },
    script: 'index.js',
    ignore: [
      'migrations/',
      'node_modules/',
      'fixtures/'
    ]
  });
});

gulp.task('nodemon:inspect', () => {
  nodemon({
    execMap: {
      js: `node --inspect${config.has('inspectPort') ? '=' + config.inspectPort : ''}`
    },
    script: 'index.js',
    ignore: [
      'migrations/',
      'node_modules/',
      'fixtures/'
    ]
  });
});

gulp.task('db:load', require('./tasks/dbLoad'));
