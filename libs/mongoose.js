/**
 * This file must be required at least ONCE.
 * After it's done, one can use require('mongoose')
 *
 * In web-app: ctx is done at init phase
 * In tests: in mocha.opts
 * In gulpfile: in beginning
 */

'use strict';

const mongoose = require('mongoose');
const beautifyUnique = require('mongoose-beautiful-unique-validation');
const config = require('config');
mongoose.Promise = Promise;

mongoose.plugin(beautifyUnique);

if (process.env.MONGOOSE_DEBUG) {
  mongoose.set('debug', true);
}

mongoose.plugin(schema => {
  if (!schema.options.toObject) {
    schema.options.toObject = {};
  }

  if (schema.options.toObject.transform === undefined) {
    schema.options.toObject.transform = (doc, ret) => {
      delete ret.__v;
      return ret;
    };
  }
});

mongoose.connect(config.mongoose.uri, config.mongoose.options);

module.exports = mongoose;
