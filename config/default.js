'use strict';

const defer = require('config/defer').deferConfig;
const path = require('path');

require('dotenv').config();

module.exports = {
  // secret data can be moved to env variables
  // or a separate config
  secret: process.env.SESSION_SECRET || 'mysecret',
  mongoose: {
    uri: process.env.MONGO_URI || 'mongodb://localhost/app',
    options: {
      server: {
        socketOptions: {
          keepAlive: 1
        },
        poolSize: 5
      }
    }
  },
  crypto: {
    hash: {
      length: 128,

      // may be slow(!): iterations = 12000 take ~60ms to generate strong password

      iterations: process.env.NODE_ENV === 'production' ? 12000 : 1
    }
  },
  template: {
    // template.root uses config.root
    root: defer(function(cfg) {
      return path.join(cfg.root, 'templates');
    })
  },
  root: process.cwd(),
  port: process.env.APP_PORT || 3001,
  inspectPort: process.env.INSPECT_PORT || 9229,
  enums: {
    execStatuses: {
      inProgress: 1,
      success: 2,
      error: 3
    }
  }
};
