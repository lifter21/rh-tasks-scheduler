**Job Scheduler**
=================

_**Objective**_ 

Provide a mini application that manages and controls remote jobs scheduling. 

_**Requirements:**_

* Web interface with a list of scheduled jobs. 

* Grid should contain the following columns: Job ID, Command Line, Host, Date Created and Date Modified, Status (of last run) and next scheduling time. It should also contain a recurrence pattern in crontab like expression. [1]

* Users can define new jobs, edit and delete existing jobs. (CRUD [2])

* When user inserts a new job, he or she should provide the desired command line that should be executed and the hostname/IP of the remote host it should be run on. They should also provide user and password to that host assuming it can be accessed via SSH. (Out of scope: connecting with PPK files). In addition, a recurrence pattern should indicate when this command line is scheduled to be executed. (Either free crontab expression or more strict definitions, like daily at 06:00, every Monday at 22:30, etc.)

* User should also be able to enable/disable jobs rather than just deleting them.

* When clicking on a certain row of this grid, user will get the history of the last executions
of this job with their status.

**Notes**

1. Backend should be implemented in Node.js.
2. DB ­ Your choice.
3. UI to be implemented in any framework you like.
4. Do not focus in polishing UI, it should be as simple as it can be as long as the described
functionality is supported. 
5. Authentication and user management ­ Out of scope.
6. Send a zip/tar file with all the sources and packages. Also provide a readme file with instructions on how to activate your application.

[1] ​http://www.nncron.ru/help/EN/working/cron­format.htm
[2] https://en.wikipedia.org/wiki/Create,_read,_update_and_delete