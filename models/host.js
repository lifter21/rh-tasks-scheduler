'use strict';

const _ = require('lodash');
const mongoose = require('mongoose');
const mongooseDelete = require('mongoose-delete');

// const Client = require('ssh2').Client;
const SSHClient = require('node-ssh');

const HostSchema = new mongoose.Schema({
  hostUri: {
    type: String,
    required: 'Host URI is required.'
  },
  port: {
    type: Number,
    min: 0,
    max: 65535, // The port number is an unsigned 16-bit integer, so 65535
    validate: {
      validator: _.isInteger,
      message: '{VALUE} is not a valid port number!'
    }
  },
  username: {
    type: String,
    required: 'Username is required'
  },
  password: {
    type: String,
    required: 'Password is required'
  },
  privateKey: {
    type: String
  }
}, {
  timestamps: true
});

// TODO: add encode/decode for password

class HostExt {
  async execCommand ({ command, cwd } = {}) {
    if (!command || !_.isString(command)) {
      return Promise.reject(new Error('Invalid command string!'));
    }

    const self = this;
    const client = new SSHClient();

    try {
      await client.connect({
        host: self.hostUri,
        port: self.port,
        username: self.username,
        password: self.password

        // privateKey: this.privateKey
      });

      console.log(self);
      console.log(client);

      const output = await client.execCommand(command, { cwd });

      return Promise.resolve(output);
    } catch (ex) {
      throw ex;
    }
  }
}

HostSchema.loadClass(HostExt);

HostSchema.statics.publicFields = [ 'hostUri', 'username', 'password', 'privateKey', 'port' ];
HostSchema.statics.secureFields = [ 'hostUri', 'userName', 'port' ];

HostSchema.plugin(mongooseDelete);

module.exports = mongoose.model('Host', HostSchema);

