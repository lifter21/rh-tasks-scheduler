'use strict';

const _ = require('lodash');
const config = require('config');
const mongoose = require('mongoose');
const mongooseDelete = require('mongoose-delete');
const parser = require('cron-parser');

const Job = require('node-schedule').Job;
const JobContainer = require('../routes/job/lib/jobs');
const Host = require('./host');
const History = require('./history');

const JobSchema = new mongoose.Schema({
  name: String,
  command: {
    type: String,
    required: 'Command string is required.'
  },
  lastExecStatus: {
    type: Number,
    enum: _.values(config.enums.execStatuses)
  },
  nextExecTime: {
    type: Date
  },
  pattern: {
    type: String,
    required: 'Pattern is required.',
    validate: {
      validator: v => {
        try {
          parser.parseExpression(v);

          return true;
        }
        catch (ex) {
          return false;
        }
      },
      message: '{VALUE} is not a valid cron pattern!'
    }
  },
  host: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Host',
    required: true
  }
}, {
  timestamps: true
});

class ScheduledJob extends Job {
  constructor(id, ...args) {
    super(...args);
    this.id = id;
  }
}

class JobExt {

  static async createAndSchedule(data) {
    const job = new this(data);

    await job.save();
    await job.schedule();

    return Promise.resolve(job);
  }

  async saveAndSchedule() {
    await this.save();
    await this.schedule();

    return Promise.resolve(this);
  }

  async schedule() {
    const self = this;

    const jobHandler = async function jobHandler() {
      try {
        const host = await Host.findOne({
          _id: self.host
        });

        if (!host) {
          return Promise.reject(new Error('Invalid host.'));
        }

        let result;
        try {
           result = await host.execCommand({
            command: self.command
          });
        } catch (e) {
          return self.stop();
        }

        const execStatus = result.stderr ? config.enums.execStatuses.success : config.enums.execStatuses.error;

        await History.create({
          job: self._id,
          execTime: new Date(),
          status: execStatus,
          output: {
            stdout: result.stdout,
            stderr: result.stderr
          }
        });

        this.lastExecStatus = execStatus;
        this.save();
      }
      catch (ex) {
        throw ex;
      }
    };

    let job = JobContainer.get(this._id);

    if (!job) {
      job = new ScheduledJob(this._id, this.name, jobHandler);

      job.on('scheduled', nextExecTime => {
        this.nextExecTime = nextExecTime;
        this.save();
      });


      job.schedule(this.pattern);
      JobContainer.add(job);
    }
    else {
      job.cancel();
      job.reschedule(this.pattern);
    }

    return Promise.resolve(this);
  }

  async deleteAndStop() {
    await this.delete();
    await this.stop();

    return Promise.resolve();
  }

  async stop() {
    const self = this;

    return new Promise(async (resolve, reject) => {
      let job = JobContainer.remove(this._id);

      if (!job) {
        self.nextExecTime = null;
        await self.save();

        return resolve();
      }

      job.on('canceled', async () => {
        self.nextExecTime = null;
        await self.save();

        setTimeout(() => {
          job = null;

          resolve();
        });
      });

      job.cancel();
    });
  }

  async restoreAndSchedule() {
    await this.restore();
    await this.schedule();

    return Promise.resolve(this);
  }
}

JobSchema.loadClass(JobExt);

// JobSchema.statics.publicGetFields = [ 'name', 'command', 'pattern', 'host', 'deleted' ];
JobSchema.statics.publicPostFields = ['name', 'command', 'pattern', 'host'];

JobSchema.plugin(mongooseDelete);

module.exports = mongoose.model('Job', JobSchema);
