'use strict';

const mongoose = require('mongoose');
const config = require('config');
const _ = require('lodash');

const HistorySchema = new mongoose.Schema({
  job: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Job'
  },
  execTime: {
    type: Date,
    required: true
  },
  status: {
    type: Number,
    enum: _.values(config.enums.execStatuses),
    required: true
  },
  output: {
    stdout: String,
    stderr: String
  }
}, {
  timestamps: true
});

module.exports = mongoose.model('History', HistorySchema);
