'use strict';
// use safe .env

const PORT = require('config').get('port');
const server = require('./server');

server.listen(PORT);
