'use strict';

exports.post = async (ctx, next) => {
  ctx.logout();

  ctx.session = null; // destroy session (!!!)

  ctx.body = 'Logged out.';
};
