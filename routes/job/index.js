'use strict';

const handlers = require('./handlers');

module.exports.init = router => {
  router
    .param('jobId', handlers.param)
    .get('/jobs', handlers.get)
    .post('/jobs', handlers.post)
    .get('/jobs/:jobId', handlers.getById)
    .put('/jobs/:jobId', handlers.put)
    .del('/jobs/:jobId', handlers.del)
    .put('/jobs/:jobId/restore', handlers.restoreDel)
    .get('/jobs/:jobId/history', handlers.getJobHistory)
    .put('/jobs/:jobId/schedule', handlers.schedule)
    .put('/jobs/:jobId/stop', handlers.stop);
};
