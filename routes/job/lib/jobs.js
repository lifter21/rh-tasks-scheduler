'use strict';

/**
 * Singleton container of running jobs
 */
class JobsContainer {
  constructor () {
    if (!JobsContainer.instance) {
      this.jobs = [];
      JobsContainer.instance = this;
    }

    return JobsContainer.instance;
  }

  add (job) {
    this.jobs.push(job);

    return job;
  }

  get (id) {
    return this.jobs.find(job => String(job.id) === String(id));
  }

  remove (id) {
    const job = this.get(id);

    if (!job) {
      return null;
    }

    return this.jobs.splice(this.jobs.indexOf(job), 1)[0];
  }
}

const instance = new JobsContainer();

Object.freeze(instance);

module.exports = instance;
