'use strict';

const mongoose = require('../../libs/mongoose');

const pick = require('lodash/pick');
const _ = require('lodash');

const Job = require('../../models/job');
const JobHistory = require('../../models/history');

exports.param = async (id, ctx, next) => {
  if (!mongoose.Types.ObjectId.isValid(id)) {
    ctx.throw(404);
  }

  ctx.jobById = await Job.findById(id);

  if (!ctx.jobById) {
    ctx.throw(404);
  }

  await next();
};

exports.get = async (ctx, next) => {
  const jobs = await Job.find({}); // .lean(), but better do it on output

  ctx.body = {
    data: jobs.map(job => job.toObject())
  };
};

exports.post = async (ctx, next) => {
  const job = await Job.createAndSchedule(pick(ctx.request.body, Job.publicPostFields));

  // userSchema.options.toObject.transform hides __v
  ctx.body = {
    data: job.toObject()
  };
};

exports.getById = async ctx => {
  ctx.body = {
    data: ctx.jobById.toObject()
  };
};

exports.put = async (ctx, next) => {
  Object.assign(ctx.jobById, pick(ctx.request.body, Job.publicPostFields));
  await ctx.jobById.saveAndSchedule();

  ctx.body = {
    data: ctx.jobById.toObject()
  };
};

exports.del = async (ctx, next) => {
  await ctx.jobById.deleteAndStop();

  ctx.body = {
    data: true
  };
};

exports.restoreDel = async (ctx, next) => {
  await ctx.jobById.restoreAndSchedule();

  ctx.body = {
    data: true
  };
};

exports.getJobHistory = async (ctx, next) => {
  const historyRecords = await JobHistory.find({ job: ctx.jobById._id });

  ctx.body = {
    data: _.map(historyRecords, rec => rec.toObject())
  };
};

exports.schedule = async (ctx, next) => {
  await ctx.jobById.schedule();

  ctx.body = {
    data: true
  };
};

exports.stop = async (ctx, next) => {
  await ctx.jobById.stop();

  ctx.body = {
    data: true
  };
};
