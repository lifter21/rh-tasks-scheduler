'use strict';

// can be split into files too
const Router = require('koa-router');
const router = new Router();

require('./job').init(router);
require('./host').init(router);

// router.post('/login', require('./login').post);
// router.post('/logout', require('./logout').post);

module.exports = router;
