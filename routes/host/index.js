'use strict';

const handlers = require('./handlers');

module.exports.init = router => {
  router
    .param('hostId', handlers.param)
    .get('/hosts', handlers.get)
    .post('/hosts', handlers.post)
    .get('/hosts/:hostId', handlers.getById);
};
