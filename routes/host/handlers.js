'use strict';

const pick = require('lodash/pick');

const mongoose = require('../../libs/mongoose');
const Host = require('../../models/host');

exports.param = async (id, ctx, next) => {
  if (!mongoose.Types.ObjectId.isValid(id)) {
    ctx.throw(404);
  }

  ctx.hostById = await Host.findById(id);

  if (!ctx.hostById) {
    ctx.throw(404);
  }

  await next();
};

exports.get = async (ctx, next) => {
  const hosts = await Host.find({}); // .lean(), but better do it on output

  ctx.body = {
    data: hosts.map(host => host.toObject())
  };

  // ctx.body = hosts.map(host => pick(host.toObject(), Host.secureFields)); // send secure fields
};

exports.post = async (ctx, next) => {
  const host = await Host.create(pick(ctx.request.body, Host.publicFields));

  // userSchema.options.toObject.transform hides __v
  ctx.body = {
    data: host.toObject()
  };
};

exports.getById = async ctx => {
  ctx.body = {
    data: ctx.hostById.toObject()
  };
};
