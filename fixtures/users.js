'use strict';

const oid = require('../libs/oid');
require('../models/user');

exports.User = [{
  _id:      oid('user'),
  email:    "user@example.com",
  displayName: 'Super user',
  password: '123456'
}, {
  _id:      oid('user-ok'),
  email:    "user-ok@example.com",
  displayName: 'Just User',
  password: '123456'
}];
