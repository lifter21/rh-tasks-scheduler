'use strict';

const passport = require('../libs/passport');

//   ctx.login(user)
//   ctx.logout()
//   ctx.isAuthenticated()
// @see https://github.com/rkusa/koa-passport/blob/master/lib/framework/koa.js
// @see https://github.com/jaredhanson/passport/blob/master/lib/middleware/initialize.js
module.exports = passport.initialize();
