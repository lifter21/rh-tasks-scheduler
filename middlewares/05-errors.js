'use strict';

module.exports = async function (ctx, next) {
  try {
    await next();
  } catch (er) {
    // let preferredType = ctx.accepts('html', 'json');

    if (er.status) {
      // could use template methods to render error page
      ctx.body = {
        error: er
      };
      ctx.status = er.status;
    } else if (er.name === 'ValidationError') {
      ctx.status = 400;

      const errors = {};

      for (const field in er.errors) {
        errors[field] = er.errors[field].message;
      }

      ctx.body = {
        errors: errors
      };
    } else {
      ctx.body = {
        error: "Error 500"
      };

      ctx.status = 500;
      console.error(er.message, er.stack);
    }
  }
};
